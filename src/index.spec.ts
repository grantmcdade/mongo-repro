import mongodb from 'mongodb';

describe('MongoDB', () => {
    it('should connect', async () => {
        await mongodb.connect('mongodb://mongo:27017');
    });
});